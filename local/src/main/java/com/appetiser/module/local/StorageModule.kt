package com.appetiser.module.local

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys
import com.appetiser.module.local.features.AppDatabase
import com.appetiser.module.local.features.music.MusicLocalSource
import com.appetiser.module.local.features.music.MusicLocalSourceImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class StorageModule {

    @Provides
    @Singleton
    fun providesSharedPreferences(application: Application): SharedPreferences {
        return if (BuildConfig.DEBUG) {
            application.getSharedPreferences(
                getDefaultPreferencesName(application),
                Context.MODE_PRIVATE
            )
        } else {
            val masterKeyAlias = MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC)
            EncryptedSharedPreferences
                .create(
                    getDefaultPreferencesName(application),
                    masterKeyAlias,
                    application,
                    EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                    EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
                )
        }
    }

    private fun getDefaultPreferencesName(application: Application): String {
        return application.packageName + "_preferences"
    }

    @Provides
    @Singleton
    fun providesUserLocalSource(appDatabase: AppDatabase): MusicLocalSource {
        return MusicLocalSourceImpl(appDatabase.musicDao())
    }
}
