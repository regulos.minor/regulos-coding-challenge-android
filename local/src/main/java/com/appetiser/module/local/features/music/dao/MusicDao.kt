package com.appetiser.module.local.features.music.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.appetiser.module.local.base.BaseDao
import com.appetiser.module.local.features.music.models.MusicDB
import io.reactivex.Single

@Dao
abstract class MusicDao : BaseDao<MusicDB> {
    @Query("SELECT * FROM music")
    abstract fun getMusics(): Single<List<MusicDB>>

    @Insert
    abstract fun insertMusic(music: MusicDB): Single<Long>

    @Insert
    abstract fun insertMusics(musics: List<MusicDB>): Single<List<Long>>
}
