package com.appetiser.module.local.features.music.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.appetiser.module.domain.models.music.Music

@Entity(tableName = MusicDB.TABLE_NAME)
data class MusicDB(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val artist: String = "",
    val trackName: String = "",
    val imageURL: String = "",
    val price: Double = 0.00,
    val genre: String = "",
    val description: String = "",
    val previewUrl: String = ""
) {
    companion object {
        const val TABLE_NAME = "music"

        fun MusicDB.toDomain(): Music {
            return Music(
                trackName = trackName,
                imageURL = imageURL,
                price = price.toString(),
                genre = genre,
                description = description,
                artist = artist,
                videoPreviewUrl = previewUrl
            )
        }

        fun List<MusicDB>.toListDomain(): List<Music> = this.map { it.toDomain() }
    }
}
