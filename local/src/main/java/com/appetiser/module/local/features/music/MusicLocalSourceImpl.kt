package com.appetiser.module.local.features.music

import com.appetiser.module.domain.models.music.Music
import com.appetiser.module.local.features.music.dao.MusicDao
import com.appetiser.module.local.features.music.models.MusicDB
import com.appetiser.module.local.features.music.models.MusicDB.Companion.toListDomain
import io.reactivex.Single
import javax.inject.Inject

class MusicLocalSourceImpl @Inject constructor(
    private val musicDao: MusicDao
) : MusicLocalSource {
    override fun getMusics(): Single<List<Music>> {
        return musicDao
            .getMusics()
            .map { musicEntities ->
                musicEntities.toListDomain()
            }
    }

    override fun saveMusics(musics: List<MusicDB>): Single<List<Long>> {
        return musicDao
            .getMusics()
            .flatMap { musicEntities ->
                if (musicEntities.isNullOrEmpty()) {
                    return@flatMap musicDao.insertMusics(musics)
                }
                val unmatchedMusics = musicEntities.filter { it.trackName !in musics.map { it.trackName } }
                return@flatMap musicDao.insertMusics(unmatchedMusics)
            }
    }
}
