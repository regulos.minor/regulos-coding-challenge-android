package com.appetiser.module.local.features.music

import com.appetiser.module.domain.models.music.Music
import com.appetiser.module.local.features.music.models.MusicDB
import io.reactivex.Single

interface MusicLocalSource {
    /**
     * get musics from db then convert result to single list
     * */
    fun getMusics(): Single<List<Music>>
    /**
     * save musics into db
     * */
    fun saveMusics(musics: List<MusicDB>): Single<List<Long>>
}
