package com.appetiser.module.local

import com.appetiser.module.local.features.music.MusicLocalSource
import com.appetiser.module.local.features.music.MusicLocalSourceImpl
import com.appetiser.module.local.features.music.dao.MusicDao
import com.appetiser.module.local.features.music.models.MusicDB.Companion.toListDomain
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.`when`

class MusicLocalSourceTest {

    private lateinit var subject: MusicLocalSource

    private val mockMusicDao: MusicDao = Mockito.mock(MusicDao::class.java)

    @Before
    fun setup() {
        subject = MusicLocalSourceImpl(musicDao = mockMusicDao)
    }

    @Test
    fun getMusic_ShouldMapMusicEntityToMusicDomain() {
        val expected = Stubs.listOfMusicEntities.toListDomain()
        `when`(mockMusicDao.getMusics()).thenReturn(Single.just(Stubs.listOfMusicEntities))

        subject
            .getMusics()
            .test()
            .assertComplete()
            .assertValue {
                it == expected
            }
    }

    @Test
    fun saveMusics_ShouldSaveMusicsWhenDBIsEmptyAndReturnNumberOfRowsInserted() {
        val expectedRows = listOf(1L,2L)
        `when`(mockMusicDao.getMusics()).thenReturn(Single.just(emptyList()))
        `when`(mockMusicDao.insertMusics(Stubs.listOfMusicEntities.take(2))).thenReturn(Single.just(expectedRows))

        subject
            .saveMusics(Stubs.listOfMusicEntities)
            .test()
            .assertComplete()
            .assertValue {
                it == expectedRows
            }
    }

    @Test
    fun saveMusics_ShouldSaveMusicsWhenDBIsNotEmptyAndReturnNumberOfRowsInserted() {
        val unmatchedMusics = Stubs.listOfMusicEntities
            .filter { it.trackName !in Stubs.listOfMusicEntities2.map { it.trackName } }
        val expectedRows = listOf<Long>()
        `when`(mockMusicDao.getMusics()).thenReturn(Single.just(Stubs.listOfMusicEntities))
        `when`(mockMusicDao.insertMusics(unmatchedMusics)).thenReturn(Single.just(expectedRows))

        subject
            .saveMusics(Stubs.listOfMusicEntities2)
            .test()
            .assertComplete()
            .assertValue {
                it == expectedRows
            }
    }
}