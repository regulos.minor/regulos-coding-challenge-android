package com.appetiser.module.data

import com.appetiser.module.data.features.music.MusicRepository
import com.appetiser.module.data.features.music.MusicRepositoryImpl
import com.appetiser.module.local.features.music.MusicLocalSource
import com.appetiser.module.local.features.music.models.MusicDB.Companion.toListDomain
import com.appetiser.module.network.features.music.MusicRemoteSource
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.`when`

class MusicRepositoryTest {
    private val mockLocalSource = Mockito.mock(MusicLocalSource::class.java)
    private val mockRemoteSource = Mockito.mock(MusicRemoteSource::class.java)
    private lateinit var subject: MusicRepository

    @Before
    fun setup() {
        subject = MusicRepositoryImpl(
            mockLocalSource,
            mockRemoteSource
        )
    }

    @Test
    fun getMusics_ShouldGetMusicsFromApiAndReturnListOfEntity() {
        val expected = Stubs.listOfMusicEntities
        `when`(mockRemoteSource.getMusics()).thenReturn(Single.just(expected))

        subject
            .getMusicsFromApi()
            .test()
            .assertComplete()
            .assertValue {
                it == expected
            }
    }

    @Test
    fun getMusicsFromDb_ShouldGetMusicsFromDBAndReturnListOfDomain() {
        val expected = Stubs.listOfMusicEntities.toListDomain()
        `when`(mockLocalSource.getMusics()).thenReturn(Single.just(expected))

        subject
            .getMusicsFromDb()
            .test()
            .assertComplete()
            .assertValue {
                it == expected
            }
    }

    @Test
    fun saveMusicsToDb_ShouldSaveMusicsToDBAndReturnNumberOfRows() {
        val expectedRows = listOf<Long>(1L, 2L)
        val entities = Stubs.listOfMusicEntities.take(2)
        `when`(mockLocalSource.saveMusics(entities)).thenReturn(Single.just(expectedRows))

        subject
            .saveMusicsToDb(entities)
            .test()
            .assertComplete()
            .assertValue {
                it == expectedRows
            }
    }
}
