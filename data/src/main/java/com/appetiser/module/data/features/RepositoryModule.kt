package com.appetiser.module.data.features

import com.appetiser.module.data.features.music.MusicRepository
import com.appetiser.module.data.features.music.MusicRepositoryImpl
import com.appetiser.module.local.features.music.MusicLocalSource
import com.appetiser.module.network.features.music.MusicRemoteSource
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {
    @Singleton
    @Provides
    fun providesMusicRepository(
        musicLocalSource: MusicLocalSource,
        musicRemoteSource: MusicRemoteSource
    ): MusicRepository {
        return MusicRepositoryImpl(musicLocalSource, musicRemoteSource)
    }
}
