package com.appetiser.module.data.features.music

import com.appetiser.module.domain.models.music.Music
import com.appetiser.module.local.features.music.MusicLocalSource
import com.appetiser.module.local.features.music.models.MusicDB
import com.appetiser.module.network.features.music.MusicRemoteSource
import io.reactivex.Single
import javax.inject.Inject

class MusicRepositoryImpl @Inject constructor(
    private val musicLocalSource: MusicLocalSource,
    private val musicRemoteSource: MusicRemoteSource
) : MusicRepository {
    override fun getMusicsFromApi(): Single<List<MusicDB>> {
        return musicRemoteSource
            .getMusics()
    }

    override fun getMusicsFromDb(): Single<List<Music>> {
        return musicLocalSource
            .getMusics()
    }

    override fun saveMusicsToDb(music: List<MusicDB>): Single<List<Long>> {
        return musicLocalSource
            .saveMusics(music)
    }
}
