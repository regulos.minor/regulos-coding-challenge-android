package com.appetiser.module.data.features.music

import com.appetiser.module.domain.models.music.Music
import com.appetiser.module.local.features.music.models.MusicDB
import io.reactivex.Single

interface MusicRepository {
    /**
     * get musics from remote source
     * then save into db
     * */
    fun getMusicsFromApi(): Single<List<MusicDB>>

    /**
     * get musics from db
     * */
    fun getMusicsFromDb(): Single<List<Music>>

    fun saveMusicsToDb(music: List<MusicDB>): Single<List<Long>>
}
