package com.appetiser.androidcodingchallenge.di.builders

import com.appetiser.androidcodingchallenge.di.scopes.FragmentScope
import com.appetiser.androidcodingchallenge.features.details.DetailsFragment
import com.appetiser.androidcodingchallenge.features.home.HomeFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilder {
    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributesHomeFragment(): HomeFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributesDetailsFragment(): DetailsFragment
}
