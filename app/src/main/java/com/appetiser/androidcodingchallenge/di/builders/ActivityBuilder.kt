package com.appetiser.androidcodingchallenge.di.builders

import com.appetiser.androidcodingchallenge.di.scopes.ActivityScope
import com.appetiser.androidcodingchallenge.features.main.MainActivity
import com.appetiser.androidcodingchallenge.features.splash.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeSplashActivity(): SplashActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): MainActivity
}
