package com.appetiser.androidcodingchallenge.features.home

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import com.appetiser.androidcodingchallenge.R
import com.appetiser.androidcodingchallenge.databinding.FragmentHomeBinding
import com.appetiser.module.common.base.BaseViewModelFragment
import com.appetiser.module.domain.models.music.Music
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy

class HomeFragment : BaseViewModelFragment<FragmentHomeBinding, HomeViewModel>(), HomeAdapter.OnItemListener {
    override fun getLayoutId(): Int = R.layout.fragment_home

    private val homeAdapter: HomeAdapter by lazy {
        return@lazy HomeAdapter()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getMusicFromDb()
        setupRecyclerView()
        setupVMObservers()
    }

    private fun setupRecyclerView() {
        binding
            .rvMusics
            .apply {
                this.adapter = homeAdapter
                homeAdapter.setItemListener(this@HomeFragment)
                this.layoutManager = GridLayoutManager(
                    requireContext(),
                    1,
                    GridLayoutManager.VERTICAL,
                    false
                )
                this.addItemDecoration(
                    DividerItemDecoration(
                        this@HomeFragment.requireContext(),
                        DividerItemDecoration.VERTICAL
                    )
                )
            }
    }

    private fun setupVMObservers() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeOn(scheduler.io())
            .subscribeBy(
                onNext = { state ->
                    handleState(state = state)
                },
                onError = { throwable ->
                    throwable.message?.let { message ->
                        showError(message = message)
                    }
                }
            )
            .addTo(disposables)
    }

    private fun handleState(state: HomeState) {
        when (state) {
            is HomeState.GettingMusicError -> {
                state.msg?.let {
                    message -> showError(message = message)
                }
            }
            is HomeState.GettingMusicSuccess -> {
                homeAdapter.setItems(items = state.musics)
            }
        }
    }

    override fun onItemClick(music: Music) {
        super.onItemClick(music)
        val action = HomeFragmentDirections.actionHomeFragmentToDetailsFragment(music = music)
        findNavController().navigate(action)
    }
}
