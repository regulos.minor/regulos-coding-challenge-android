package com.appetiser.androidcodingchallenge.features.home

import androidx.recyclerview.widget.RecyclerView
import com.appetiser.androidcodingchallenge.databinding.ItemMusicBinding
import com.appetiser.androidcodingchallenge.utils.loadImage
import com.appetiser.module.domain.models.music.Music

class HomeViewHolder(private val binding: ItemMusicBinding) : RecyclerView.ViewHolder(binding.root) {
    var listener: HomeAdapter.OnItemListener? = null

    fun bind(data: Music) {
        binding.music = data
        loadImage(data)
        binding
            .root
            .setOnClickListener {
                listener?.onItemClick(data)
            }
    }

    private fun loadImage(data: Music) {
        binding
            .imgMusic
            .loadImage(
                data
                    .imageURL
                    .replace(
                        "100x100",
                        "600x600"
                    )
            )
    }
}
