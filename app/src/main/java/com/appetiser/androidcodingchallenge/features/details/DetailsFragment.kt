package com.appetiser.androidcodingchallenge.features.details

import android.os.Bundle
import android.view.View
import com.appetiser.androidcodingchallenge.R
import com.appetiser.androidcodingchallenge.databinding.FragmentMusicDetailBinding
import com.appetiser.module.common.base.BaseFragment
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem

class DetailsFragment : BaseFragment<FragmentMusicDetailBinding>() {
    private var exoplayer: ExoPlayer? = null

    private val music by lazy {
        return@lazy DetailsFragmentArgs.fromBundle(requireArguments()).music
    }
    override fun getLayoutId(): Int = R.layout.fragment_music_detail

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupExoPlayer()
        setupViews()
    }

    private fun setupViews() {
        binding.music = music
        binding
            .playerView
            .apply {
                val mediaItem = MediaItem.fromUri(music.videoPreviewUrl)
                player = exoplayer
                exoplayer!!.setMediaItem(mediaItem)
                exoplayer!!.prepare()
                exoplayer!!.play()
            }
    }

    private fun setupExoPlayer() {
        exoplayer = ExoPlayer.Builder(requireContext()).build()
    }

    override fun onDestroyView() {
        exoplayer!!.release()
        exoplayer = null
        super.onDestroyView()
    }

    override fun onPause() {
        exoplayer!!.pause()
        super.onPause()
    }

    override fun onResume() {
        exoplayer!!.play()
        super.onResume()
    }
}
