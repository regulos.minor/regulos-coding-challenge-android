package com.appetiser.androidcodingchallenge.features.splash

import com.appetiser.module.common.base.BaseViewModel
import com.appetiser.module.data.features.music.MusicRepository
import com.appetiser.module.local.features.music.models.MusicDB
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class SplashViewModel @Inject constructor(
    private val musicRepository: MusicRepository
) : BaseViewModel() {

    private val _state by lazy { PublishSubject.create<SplashState>() }
    val state: Observable<SplashState> = _state

    fun getAndSaveMusics() {
        musicRepository
            .getMusicsFromApi()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onSuccess = { musics ->
                    saveMusicToDb(musics)
                },
                onError = { throwable ->
                    _state.onNext(SplashState.SavingMusicError(msg = throwable.message))
                }
            )
            .addTo(disposables)
    }

    private fun saveMusicToDb(musics: List<MusicDB>) {
        musicRepository
            .saveMusicsToDb(musics)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onSuccess = {
                    _state.onNext(SplashState.SavingMusicSuccess)
                },
                onError = { throwable ->
                    _state.onNext(SplashState.SavingMusicError(msg = throwable.message))
                }
            )
            .addTo(disposables)
    }
}
