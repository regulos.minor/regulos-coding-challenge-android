package com.appetiser.androidcodingchallenge.features.main

import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.appetiser.androidcodingchallenge.R
import com.appetiser.androidcodingchallenge.databinding.ActivityMainBinding
import com.appetiser.module.common.base.BaseActivity

class MainActivity : BaseActivity<ActivityMainBinding>() {
    override fun getLayoutId(): Int = R.layout.activity_main

    fun navController(): NavController = findNavController(R.id.nav_host_fragment)
}
