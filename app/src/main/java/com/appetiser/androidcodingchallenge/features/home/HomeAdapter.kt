package com.appetiser.androidcodingchallenge.features.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.appetiser.androidcodingchallenge.databinding.ItemMusicBinding
import com.appetiser.module.domain.models.music.Music

class HomeAdapter : RecyclerView.Adapter<HomeViewHolder>() {
    private val items = mutableListOf<Music>()
    private var listener: OnItemListener? = null

    interface OnItemListener {
        fun onItemClick(music: Music) {}
    }

    fun setItemListener(listener: OnItemListener) {
        this.listener = listener
    }

    fun setItems(items: List<Music>) {
        if (this.items.isNotEmpty()) this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    fun getItem(position: Int) = items[position]

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemMusicBinding.inflate(inflater, parent, false)
        val viewHolder = HomeViewHolder(binding)
        viewHolder.listener = this.listener
        return viewHolder
    }

    override fun onBindViewHolder(holder: HomeViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun getItemCount(): Int = items.size
}
