package com.appetiser.androidcodingchallenge.features.home

import com.appetiser.module.domain.models.music.Music

sealed class HomeState {
    data class GettingMusicSuccess(val musics: List<Music>) : HomeState()
    /**
     * state for getting music from db with error
     * */
    data class GettingMusicError(val msg: String?) : HomeState()
}
