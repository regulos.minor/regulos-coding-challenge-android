package com.appetiser.androidcodingchallenge.features.home

import com.appetiser.module.common.base.BaseViewModel
import com.appetiser.module.data.features.music.MusicRepository
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class HomeViewModel @Inject constructor(
    private val musicRepository: MusicRepository
) : BaseViewModel() {

    private val _state by lazy { PublishSubject.create<HomeState>() }
    val state: Observable<HomeState> = _state

    fun getMusicFromDb() {
        musicRepository
            .getMusicsFromDb()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onSuccess = { musics ->
                    _state.onNext(HomeState.GettingMusicSuccess(musics = musics.sortedBy { it.trackName }))
                },
                onError = { throwable ->
                    _state.onNext(HomeState.GettingMusicError(msg = throwable.message))
                }
            )
            .addTo(disposables)
    }
}
