package com.appetiser.androidcodingchallenge.features.splash

import android.os.Bundle
import com.appetiser.androidcodingchallenge.R
import com.appetiser.androidcodingchallenge.databinding.ActivitySplashBinding
import com.appetiser.androidcodingchallenge.features.main.MainActivity
import com.appetiser.module.common.base.BaseViewModelActivity
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy

class SplashActivity : BaseViewModelActivity<ActivitySplashBinding, SplashViewModel>() {
    override fun getLayoutId(): Int = R.layout.activity_splash

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.getAndSaveMusics()
        setupVMObservers()
    }
    private fun setupVMObservers() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = { state ->
                    handleState(state = state)
                },
                onError = { throwable ->
                    throwable.message?.let { message ->
                        showError(message)
                    }
                }
            )
            .addTo(disposables)
    }

    private fun handleState(state: SplashState) {
        when (state) {
            is SplashState.SavingMusicSuccess -> {
                finishActivity()
            }
            is SplashState.SavingMusicError -> {
                state.msg?.let {
                    showError(it)
                    finishActivity()
                }
            }
        }
    }

    private fun finishActivity() {
        navigateToActivity(MainActivity::class.java)
        finish()
    }
}
