package com.appetiser.androidcodingchallenge.utils

import android.widget.ImageView
import com.appetiser.androidcodingchallenge.R
import com.bumptech.glide.Glide

fun ImageView.loadImage(url: String) {
    Glide
        .with(this.context)
        .load(url)
        .centerCrop()
        .placeholder(R.drawable.ic_music)
        .into(this)
}
