package com.appetiser.androidcodingchallenge

import com.appetiser.androidcodingchallenge.core.BaseViewModelTest
import com.appetiser.androidcodingchallenge.features.splash.SplashState
import com.appetiser.androidcodingchallenge.features.splash.SplashViewModel
import com.appetiser.module.data.features.music.MusicRepository
import io.reactivex.Observer
import io.reactivex.Single
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.Mockito.times
import java.net.UnknownHostException

class SplashViewModelTest : BaseViewModelTest() {
    private val mockStateObserver = Mockito.mock(Observer::class.java) as Observer<SplashState>
    private val mockRepository = Mockito.mock(MusicRepository::class.java)
    private lateinit var subject: SplashViewModel

    @Before
    fun setup() {
        subject = SplashViewModel(mockRepository)
        subject.schedulers = schedulers
        subject.state.subscribe(mockStateObserver)
    }

    @Test
    fun getAndSaveMusics_ShouldGetMusicsFromApiThenSaveToDB() {
        val expectedState = SplashState.SavingMusicSuccess
        val insertedData = Stubs.listOfMusicEntities.take(2)
        val expectedRows = listOf<Long>(1L, 2L)

        `when`(mockRepository.getMusicsFromApi()).thenReturn(Single.just(insertedData))
        `when`(mockRepository.saveMusicsToDb(insertedData)).thenReturn(Single.just(expectedRows))

        subject.getAndSaveMusics()
        testScheduler.triggerActions()

        ArgumentCaptor.forClass(SplashState::class.java).run {
            Mockito
                .verify(
                    mockStateObserver,
                    times(1)
                )
                .onNext(capture())

            assertTrue(value is SplashState.SavingMusicSuccess)
            assertEquals(expectedState, value)
        }
    }

    @Test
    fun getAndSaveMusics_ShouldThrowErrorWhenGettingMusicsFromApiWithNoInternet() {
        val throwable = UnknownHostException()
        val expectedState = SplashState.SavingMusicError(throwable.message)

        `when`(mockRepository.getMusicsFromApi()).thenReturn(Single.error(throwable))

        subject.getAndSaveMusics()
        testScheduler.triggerActions()

        ArgumentCaptor.forClass(SplashState::class.java).run {
            Mockito
                .verify(
                    mockStateObserver,
                    times(1)
                )
                .onNext(capture())

            assertEquals(expectedState, value)
        }
    }
}
