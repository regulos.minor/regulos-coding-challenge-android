package com.appetiser.androidcodingchallenge

import com.appetiser.module.local.features.music.models.MusicDB

object Stubs {
    var listOfMusicEntities = listOf<MusicDB>(
        MusicDB(
            id = 1,
            artist = "Regulos Minor",
            trackName = "Patayin sa sindak si barbara",
            imageURL = "image.com.au",
            price = 1.0,
            genre = "horror",
            description = "horror movie",
            previewUrl = "preview.com.au"
        ),
        MusicDB(
            id = 2,
            artist = "Regulos Minor",
            trackName = "Patayin sa sindak si barbara 2",
            imageURL = "image2.com.au",
            price = 2.0,
            genre = "horror",
            description = "horror movie 2",
            previewUrl = "preview2.com.au"
        )
    )
}
