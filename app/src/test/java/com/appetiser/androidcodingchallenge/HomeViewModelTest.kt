package com.appetiser.androidcodingchallenge

import com.appetiser.androidcodingchallenge.core.BaseViewModelTest
import com.appetiser.androidcodingchallenge.features.home.HomeState
import com.appetiser.androidcodingchallenge.features.home.HomeViewModel
import com.appetiser.module.data.features.music.MusicRepository
import com.appetiser.module.local.features.music.models.MusicDB.Companion.toListDomain
import io.reactivex.Observer
import io.reactivex.Single
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.Mockito.times

class HomeViewModelTest : BaseViewModelTest() {
    private val mockStateObserver = Mockito.mock(Observer::class.java) as Observer<HomeState>
    private val mockRepository = Mockito.mock(MusicRepository::class.java)
    private lateinit var subject: HomeViewModel

    @Before
    fun setup() {
        subject = HomeViewModel(mockRepository)
        subject.schedulers = schedulers
        subject.state.subscribe(mockStateObserver)
    }

    @Test
    fun getMusicsFromDb_ShouldGetMusicsFromDB() {
        val expectedData = Stubs.listOfMusicEntities.toListDomain()
        val expectedState = HomeState.GettingMusicSuccess(expectedData)

        `when`(mockRepository.getMusicsFromDb()).thenReturn(Single.just(expectedData))
        subject.getMusicFromDb()
        testScheduler.triggerActions()

        ArgumentCaptor.forClass(HomeState::class.java).run {
            Mockito
                .verify(
                    mockStateObserver,
                    times(1)
                )
                .onNext(capture())

            assertTrue(value is HomeState.GettingMusicSuccess)
            assertEquals(expectedState, value)
        }
    }
}
