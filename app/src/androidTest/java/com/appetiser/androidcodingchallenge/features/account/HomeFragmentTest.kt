package com.appetiser.androidcodingchallenge.features.account

import androidx.fragment.app.testing.FragmentScenario
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.navigation.Navigation
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.internal.runner.junit4.statement.UiThreadStatement.runOnUiThread
import com.appetiser.androidcodingchallenge.R
import com.appetiser.androidcodingchallenge.features.home.HomeFragment
import com.appetiser.androidcodingchallenge.features.home.HomeViewHolder
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class HomeFragmentTest {

    private lateinit var homeFragment: FragmentScenario<HomeFragment>
    private lateinit var navController: TestNavHostController

    @Before
    fun setup() {
        homeFragment = launchFragmentInContainer(themeResId = R.style.Theme_Baseplate)
        navController = TestNavHostController(ApplicationProvider.getApplicationContext())
    }

    @Test
    fun ifHomeFragmentIsLaunched_itShouldDisplayRecyclerView() {
        onView(withId(R.id.rvMusics)).check(matches(isDisplayed()))
    }

    @Test
    fun ifMusicItemIsClicked_itShouldNavigateToMusicDetailFragment() {
        with(navController) {
            homeFragment.onFragment { fragment ->
                setGraph(R.navigation.main_graph)
                Navigation.setViewNavController(fragment.requireView(), this)
            }
            onView(withId(R.id.rvMusics)).perform(actionOnItemAtPosition<HomeViewHolder>(0, click()))
            assertNotNull(currentDestination?.id)
            assertEquals(currentDestination?.id, R.id.detailsFragment)
        }
    }

    @Test
    fun ifFromDetailsFragment_itShouldNavigateBackToHomeFragment() {
        with(navController) {
            homeFragment.onFragment { homeFragment ->
                setGraph(R.navigation.main_graph)
                Navigation.setViewNavController(homeFragment.requireView(), this)
            }
            onView(withId(R.id.rvMusics)).perform(actionOnItemAtPosition<HomeViewHolder>(0, click()))
            assertNotNull(currentDestination?.id)
            assertEquals(currentDestination?.id, R.id.detailsFragment)
            runOnUiThread {
                popBackStack()
            }
            onView(withId(R.id.rvMusics)).check(matches(isDisplayed()))
        }
    }

    @Test
    fun ifHomeFragmentIsLaunched_argumentsMustBeNull() {
        homeFragment.onFragment { homeFragment ->
            assertNull(homeFragment.arguments)
        }
    }
}
