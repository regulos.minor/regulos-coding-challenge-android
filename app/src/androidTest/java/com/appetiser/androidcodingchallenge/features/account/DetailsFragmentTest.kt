package com.appetiser.androidcodingchallenge.features.account

import android.os.Bundle
import androidx.fragment.app.testing.FragmentScenario
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.appetiser.androidcodingchallenge.features.details.DetailsFragment
import org.junit.Before
import org.junit.runner.RunWith
import com.appetiser.androidcodingchallenge.R
import com.appetiser.module.domain.models.music.Music
import junit.framework.Assert.assertNotNull
import org.junit.Test

@RunWith(AndroidJUnit4::class)
@LargeTest
class DetailsFragmentTest {

    private lateinit var detailsFragment: FragmentScenario<DetailsFragment>
    private lateinit var navController: TestNavHostController

    @Before
    fun setup() {
        val music = Music(
            artist = "",
            trackName = "",
            imageURL = "",
            price = "",
            genre = "",
            description = "",
            videoPreviewUrl = ""
        )
        val bundle = Bundle().apply {
            this.putParcelable("music", music)
        }
        detailsFragment = launchFragmentInContainer(themeResId = R.style.Theme_Baseplate, fragmentArgs = bundle)
        navController = TestNavHostController(ApplicationProvider.getApplicationContext())
    }

    @Test
    fun ifDetailsFragmentIsLaunch_itShouldDisplayExoPlayer() {
        onView(withId(R.id.playerView)).check(matches(isDisplayed()))
    }

    @Test
    fun ifDetailsFragmentIsLaunch_argumentMustNotNull() {
        detailsFragment.onFragment { detailsFragment ->
            assertNotNull(detailsFragment.arguments)
        }
    }
}
