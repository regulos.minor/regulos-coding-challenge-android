package com.appetiser.module.domain.models.music

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Music(
    val artist: String = "",
    val trackName: String = "",
    val imageURL: String = "",
    val price: String = "",
    val genre: String,
    val description: String = "",
    val videoPreviewUrl: String = ""
): Parcelable