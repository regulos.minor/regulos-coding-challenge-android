Programming language: Kotlin

Appetiser Android Coding Challenge
Get all musics from itunes...

Tools: Dagger2, RxJava2, Architecture components

Design pattern used: Repository pattern:

Advantages:
- Clean, Maintainable and Reusable code
- Reduces redundancy of code

Disadvantages:
- Requires new repository for each entity
- Overkill for small applications
