object TestLibs {
    //Test Libs
    val androidXJUnit = "androidx.test.ext:junit:${Versions.androidXJunitVersion}"
    val archCoreTesting = "androidx.arch.core:core-testing:${Versions.archCompTestVersion}"
    val espresso = "androidx.test.espresso:espresso-core:${Versions.espressoVersion}"
    val junit = "junit:junit:${Versions.junitVersion}"
    val mockito = "org.mockito:mockito-inline:${Versions.mockitoVersion}"
    val testRules = "androidx.test:rules:${Versions.testRulesVersion}"
    val testRunner = "androidx.test:runner:${Versions.testRunnerVersion}"
    val fragmentTest = "androidx.fragment:fragment-testing:${Versions.fragmentTestVersion}"
    val navigationTest = "androidx.navigation:navigation-testing:${Versions.navigationVersion}"
}
