package com.appetiser.module.common

const val SNACKBAR_DURATION = 3000
const val PHOTO_MAX_DIMENSION = 1080f
const val PHOTO_COMPRESSION_QUALITY = 100
