package com.appetiser.module.network.base.response

data class BaseResponse<T>(
    val resultCount: Int,
    val results: T
)
