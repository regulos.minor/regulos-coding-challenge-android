package com.appetiser.module.network.features.music

import com.appetiser.module.local.features.music.models.MusicDB
import io.reactivex.Single

interface MusicRemoteSource {
    /**
     * get musics from api
     * */
    fun getMusics(): Single<List<MusicDB>>
}
