package com.appetiser.module.network.features.music.models

import com.appetiser.module.domain.models.music.Music
import com.appetiser.module.local.features.music.models.MusicDB
import com.google.gson.annotations.SerializedName

data class MusicDTO(
    val trackName: String?,
    val artistName: String,
    @SerializedName("artworkUrl100")
    val imageURL: String,
    @SerializedName("trackPrice")
    val price: Double,
    @SerializedName("primaryGenreName")
    val genre: String,
    @SerializedName("longDescription")
    val description: String?,
    @SerializedName("previewUrl")
    val videoPreviewURL: String?
) {
    companion object {
        fun MusicDTO.toDomain(): Music {
            return with(this) {
                Music(
                    trackName = trackName ?: "",
                    artist = artistName,
                    imageURL = imageURL,
                    price = price.toString(),
                    genre = genre,
                    description = description ?: "",
                    videoPreviewUrl = videoPreviewURL ?: ""
                )
            }
        }

        fun List<MusicDTO>.toListDomain(): List<Music> {
            return this.map { it.toDomain() }
        }

        fun MusicDTO.toEntity(): MusicDB {
            return with(this) {
                MusicDB(
                    artist = artistName,
                    trackName = trackName ?: "",
                    imageURL = imageURL,
                    price = price,
                    genre = genre,
                    description = description ?: "",
                    previewUrl = videoPreviewURL ?: ""
                )
            }
        }

        fun List<MusicDTO>.toListEntity(): List<MusicDB> {
            return this.map { it.toEntity() }
        }
    }
}
