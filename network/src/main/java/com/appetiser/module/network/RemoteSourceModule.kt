package com.appetiser.module.network

import com.appetiser.module.network.features.BaseplateApiServices
import com.appetiser.module.network.features.music.MusicRemoteSource
import com.appetiser.module.network.features.music.MusicRemoteSourceImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RemoteSourceModule {

    @Provides
    @Singleton
    fun providesAuthRemoteSource(
        apiServices: BaseplateApiServices
    ): MusicRemoteSource = MusicRemoteSourceImpl(apiServices)
}
