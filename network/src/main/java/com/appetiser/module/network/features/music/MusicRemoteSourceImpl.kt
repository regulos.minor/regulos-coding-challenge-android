package com.appetiser.module.network.features.music

import com.appetiser.module.local.features.music.models.MusicDB
import com.appetiser.module.network.features.BaseplateApiServices
import com.appetiser.module.network.features.music.models.MusicDTO.Companion.toListEntity
import io.reactivex.Single
import javax.inject.Inject

class MusicRemoteSourceImpl @Inject constructor(
    private val apiService: BaseplateApiServices
) : MusicRemoteSource {
    override fun getMusics(): Single<List<MusicDB>> {
        return apiService
            .getMusics(term = "star", country = "au", media = "all")
            .map { response ->
                response.results.toListEntity()
            }
    }
}
