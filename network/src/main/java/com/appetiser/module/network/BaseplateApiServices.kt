package com.appetiser.module.network.features

import com.appetiser.module.network.base.response.BaseResponse
import com.appetiser.module.network.features.music.models.MusicDTO
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface BaseplateApiServices {
    @GET("search")
    fun getMusics(
        @Query("term") term: String,
        @Query("country") country: String,
        @Query("media") media: String
    ): Single<BaseResponse<List<MusicDTO>>>
}
