package com.appetiser.module.network

import com.appetiser.module.network.features.BaseplateApiServices
import com.appetiser.module.network.features.music.MusicRemoteSource
import com.appetiser.module.network.features.music.MusicRemoteSourceImpl
import com.appetiser.module.network.features.music.models.MusicDTO.Companion.toListEntity
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.`when`

class MusicRemoteSourceTest {
    private val mockApi = Mockito.mock(BaseplateApiServices::class.java)

    private lateinit var subject: MusicRemoteSource

    @Before
    fun setup() {
        subject = MusicRemoteSourceImpl(mockApi)
    }

    @Test
    fun getMusics_ShouldMapMusicDTOToMusicEntity() {
        val expected = Stubs.networkResponse.results.toListEntity()
        `when`(mockApi.getMusics(term = "star", country = "au", media = "all"))
            .thenReturn(Single.just(Stubs.networkResponse))

        subject
            .getMusics()
            .test()
            .assertComplete()
            .assertValue {
                it == expected
            }
    }
}
