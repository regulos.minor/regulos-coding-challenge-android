package com.appetiser.module.network

import com.appetiser.module.network.base.response.BaseResponse
import com.appetiser.module.network.features.music.models.MusicDTO

object Stubs {
    var networkResponse = BaseResponse<List<MusicDTO>>(
        resultCount = 1,
        results = listOf()
    )
}
